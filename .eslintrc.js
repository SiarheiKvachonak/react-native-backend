module.exports =  {
  parser:  'babel-eslint',
  extends:  [
    'airbnb-base',
    "prettier",
  ],
  "plugins": [
    "prettier"
  ],
  env:  {
    node:  true,
  },
  rules:  {
    'no-console': 0,
    'max-len': ["error", { "code": 120 }],
    'linebreak-style': 0,
    'no-param-reassign': ["error", { "props": false }],
    'no-underscore-dangle': ["error", { "allow": ["_id"] }]
  },
};
