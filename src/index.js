require('./models/User');
require('./models/Product');
require('./models/Basket');

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')

const authRoutes = require('./routes/authRouts');
const paymentRoutes = require('./routes/paymentRouts');
const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');
// middlewares
const requireAuth = require('./middleware/authorization');

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello, seems backend works!');
});
app.use(authRoutes);
app.use(productRoutes);

app.use(requireAuth);

app.use(userRoutes);
app.use(paymentRoutes);

const mongoUri = process.env.MONGODB_URI;

mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

mongoose.connection.on('connected', () => {
  console.log('Connected to MDB');
});

mongoose.connection.on('error', (err) => {
  console.error('Error connecting to MDB', err);
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
