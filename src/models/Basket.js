const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
  },
  totalPrice: Number,
  count: Number,
});

const basketSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  basket: [itemSchema],
  total: Number,
  count: Number,
});

mongoose.model('Basket', basketSchema);
