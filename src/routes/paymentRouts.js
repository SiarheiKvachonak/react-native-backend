// libraries
const express = require('express');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const router = express.Router();

router.post('/pay', async (req, res) => {
  await stripe.paymentIntents.create({
    ...req.body,
    confirm: true,
    error_on_requires_action: true,
  }, (err, paymentIntent) => {
    if (err) {
      return res.status(400).json({error: 'Can\'t get paymentIntent. Your purchase hasn\'t been paid.'});
    }
    return res.json({
      setupIntentId: paymentIntent.client_secret,
    });
  });
});

module.exports = router;
