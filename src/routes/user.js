const express = require('express');
const mongoose = require('mongoose');

const Basket = mongoose.model('Basket');

const router = express.Router();

router.get('/basket', async (req, res) => {
  try {
    const response = await Basket.findOne({ userId: req.user._id })
      .populate('basket.productId')
      .exec();

    return res.send(response);
  } catch (err) {
    return res.status(422).json({error: 'Can\'t show user basket.'});
  }
});

router.patch('/basket', async (req, res) => {
  const { items = [], total, count } = req.body;

  const basketItems = items.map(({ _id, total: itemsCount, totalPrice }) => ({
    productId: _id,
    count: itemsCount,
    totalPrice,
  }));

  try {
    const result = await Basket.findOneAndUpdate({
      userId: req.user._id,
    }, { basket: basketItems, total, count });

    if (!result) {
      const basket = new Basket({
        userId: req.user._id, basket: basketItems, total, count,
      });

      await basket.save();
      return res.send(basket);
    }

    return res.send(result);
  } catch (err) {
    return res.status(422).json({error: "The basket hasn't been updated."});
  }
});

module.exports = router;
