const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = mongoose.model('User');

const router = express.Router();

router.post('/signup', async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = new User({ email, password });
    await user.save();

    const token = jwt.sign({ userId: user._id }, process.env.TOKEN_SECRET_KEY);
    return res.send({ token });
  } catch (err) {
    let userMessage = 'Something went wrong!';

    if (err.code === 11000) {
      userMessage = 'Current user already registered!';
    }
    return res.status(422).json({error: userMessage});
  }
});

router.post('/signin', async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).json({error: 'Must provide email and password'});
  }

  const user = await User.findOne({ email });

  if (!user) {
    return res.status(422).json({error: 'User is not exist.'});
  }

  try {
    await user.comparePassword(password);
    const token = jwt.sign({ userId: user._id }, process.env.TOKEN_SECRET_KEY);

    return res.send({ token, isAdmin: user.isAdmin });
  } catch (err) {
    return res.status(422).json({error: 'Invalid password or email'});
  }
});

module.exports = router;
