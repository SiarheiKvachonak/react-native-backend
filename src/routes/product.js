const express = require('express');
const mongoose = require('mongoose');
const requireAuth = require('../middleware/authorization');

const Product = mongoose.model('Product');

const router = express.Router();

router.get('/products', async (req, res) => {
  try {
    const response = await Product.find({});
    return res.send(response);
  } catch (err) {

    return res.status(422).json({error: 'Something went wrong. Please reload your app.'});
  }
});

router.post('/product', requireAuth, async (req, res) => {
 const { title, description, image, price } = req.body;

  try {
    const product = new Product({ title, description, image, price });
    const response = await product.save();
    return res.send(response);
  } catch (err) {
    return res.status(422).json({error: 'Can\'t add current product'});
  }
});

module.exports = router;
