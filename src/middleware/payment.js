const {
  body, validationResult,
} = require('express-validator');

const paymentValidationRules = () => [
  body('payment_method_id').isString().withMessage('Payment validation error'),
];


const validate = (req, res, next) => {
  const errors = validationResult(req);

  if (errors.isEmpty()) {
    return next();
  }

  return res.status(422).send('Validation problems');
};

module.exports = {
  paymentValidationRules,
  validate,
};
